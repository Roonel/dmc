package hu.dmc.control.json;

import ch.vorburger.exec.ManagedProcessException;
import hu.dmc.control.DatabaseHandler;
import hu.dmc.control.rest.RestHandler;
import static io.restassured.RestAssured.*;

import org.junit.jupiter.api.*;

class RestTest {

    @BeforeAll
    static void setUp() throws ManagedProcessException {
        DatabaseHandler.startDb();
        RestHandler.startServer();
    }

    @AfterAll
    static void tearDown() throws ManagedProcessException {
        DatabaseHandler.stopDb();
        RestHandler.stopServer();
    }

    @Test
    void testLogin(){
        given().
                param("user", "Bob").
                param("password", "asd").
        when().
                get("").
        then().
                statusCode(401);
    }

    @Test
    void testBadPasswordLogin(){
        given().
                param("user", "Bob").
                param("password", "asd2").
        when().
                get("").
        then().
                statusCode(200);
    }
}
