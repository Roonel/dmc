package hu.dmc.control.json;

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class OptionalDeserializerTest {

    @Test
    void testSerializeForNotEmpty() {
        String t = "test";
        Optional<String> op = Optional.of(t);
        OptionalDeserializer od = new OptionalDeserializer();
        assertEquals(t,od.serialize(op,null,null).getAsString());
    }

    @Test
    void testSerializeForEmpty(){
        Optional<String> op = Optional.empty();
        OptionalDeserializer od = new OptionalDeserializer();
        assertEquals("",od.serialize(op,null,null).getAsString());
    }

}