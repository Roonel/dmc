package hu.dmc;

import ch.vorburger.exec.ManagedProcessException;
import hu.dmc.control.DatabaseHandler;
import hu.dmc.control.json.ModuleDataLoader;
import hu.dmc.control.rest.RestHandler;
import hu.dmc.control.ui.ModuleController;
import hu.dmc.control.util.ResourceBundleFlow;
import io.datafx.controller.ViewConfiguration;
import io.datafx.controller.flow.FlowException;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

public class Main extends Application {
    private ProgressBar bar;
    private Label loadingCurrently;
    private ResourceBundle resourceBundle;

    public void start(Stage splashStage) throws Exception {
        String lang = System.getProperty("lang");
        if (StringUtils.isNotBlank(lang)) {
            try {
                resourceBundle = ResourceBundle.getBundle("uiRes", new Locale(lang));
            } catch (Exception e) {
                resourceBundle = ResourceBundle.getBundle("uiRes", Locale.ENGLISH);
            }
        } else {
            resourceBundle = ResourceBundle.getBundle("uiRes", Locale.ENGLISH);
        }
        splashStage.initStyle(StageStyle.UNDECORATED);
        splashStage.setScene(createPreloaderScene());
        splashStage.getIcons().add(new Image("/icon.png"));
        splashStage.show();

        new Thread(() -> {
                Platform.runLater(() -> loadingCurrently.setText(resourceBundle.getString("label.initServer")));
                RestHandler.startServer();

                try {
                    Platform.runLater(() -> loadingCurrently.setText(resourceBundle.getString("label.initDatabase")));
                    DatabaseHandler.startDb();
                    ModuleDataLoader.loadDatas();
                } catch (ManagedProcessException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Platform.runLater(() -> {
                    try {
                        startApplication();
                        splashStage.hide();
                    } catch (FlowException e) {
                        e.printStackTrace();
                    }
                });
        }).start();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void startApplication() throws FlowException {
        Stage primaryStage = new Stage();
        ViewConfiguration viewConfiguration = new ViewConfiguration();
        viewConfiguration.setResources(resourceBundle);
        primaryStage.getIcons().add(new Image("/icon.png"));

        new ResourceBundleFlow(ModuleController.class, viewConfiguration)
                .withGlobalBackAction("backAction")
                .startInStage(primaryStage);

        primaryStage.getScene().getStylesheets().add("/dla-base.css");
    }

    private Scene createPreloaderScene() {
        bar = new ProgressBar();
        BorderPane p = new BorderPane();
        VBox box = new VBox(10);
        box.setFillWidth(true);
        box.setAlignment(Pos.CENTER);
        loadingCurrently = new Label();
        box.getChildren().addAll(loadingCurrently, bar);
        p.setCenter(box);
        return new Scene(p, 300, 150);
    }

    @Override
    public void stop() throws Exception {
        RestHandler.stopServer();
        DatabaseHandler.stopDb();
    }
}
