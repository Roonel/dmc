package hu.dmc.model;

import hu.dmc.shared.model.Block;
import io.datafx.controller.injection.scopes.FlowScoped;
import lombok.Data;
import hu.dmc.shared.model.generated.test.test.module.Module;
import hu.dmc.shared.model.generated.test.test.saved_template.SavedTemplate;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public  @Data class DataContext {
    private static DataContext instance;
    private Module module;
    private Block selectedBlock;
    private SavedTemplate savedTemplate;
    private Integer templateType;
    private String hostAddress;
    private double[] viewDividers;
    private double[] versionDividers;

    private DataContext(){
    }

    public static DataContext getInstance(){
        if(instance == null){
            instance = new DataContext();
        }
        return instance;
    }

}
