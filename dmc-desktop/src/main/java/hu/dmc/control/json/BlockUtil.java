package hu.dmc.control.json;

import hu.dmc.shared.model.Block;
import hu.dmc.shared.model.Element;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * Used for generating GridPanes for the UI.
 */
public class BlockUtil {

    /**
     * Generates a GridPane object from the given source, containing all the bound data.
     * @param mb contains the element list and the dataMap
     * @param readOnly readonly
     * @return this GridPane is built from the element list and filled from the dataMap
     */
    public static GridPane getGridPane(Block mb, Boolean readOnly) {
        GridPane gridPane = new GridPane();
        gridPane.setHgap(5);
        gridPane.setVgap(5);
        gridPane.setPadding(new Insets(5));
        for (Element element : mb.getElementList()) {
            switch (element.getElementType()) {
                case LABEL: {
                    Label l = new Label();
                    l = (Label) setThingsUp(l, element);
                    String value = mb.getDataMap().get(l.getId());
                    l.setText(element.getText());
                    gridPane.getChildren().add(l);
                }
                break;
                case TEXT: {
                    TextField t = new TextField();
                    t = (TextField) setThingsUp(t, element);
                    String value = mb.getDataMap().get(t.getId());
                    t.setText(StringUtils.isEmpty(value) ? "" : value);
                    t.setPromptText(element.getText());
                    t.setEditable(!readOnly && element.getEditable());
                    gridPane.getChildren().addAll(t);
                }
                break;
                case AREA: {
                    TextArea a = new TextArea();
                    a = (TextArea) setThingsUp(a, element);
                    String value = mb.getDataMap().get(a.getId());
                    a.setText(StringUtils.isEmpty(value) ? "" : value);
                    a.setPromptText(element.getText());
                    a.setEditable(!readOnly && element.getEditable());
                    a.setPrefRowCount(3);
                    a.setWrapText(true);
                    gridPane.getChildren().addAll(a);
                }
            }
        }
        try {
            Method method = gridPane.getClass().getDeclaredMethod("getNumberOfColumns");
            method.setAccessible(true);
            Integer rows = (Integer) method.invoke(gridPane);
            for (Integer i = 0; i < rows; i++) {
                ColumnConstraints cc = new ColumnConstraints();
                cc.setPercentWidth(100 / rows);
                gridPane.getColumnConstraints().add(cc);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return gridPane;
    }

    private static Node setThingsUp(Node n, Element e) {
        n.setId(e.getId());
        n.getStyleClass().addAll(e.getStyle());
        GridPane.setRowIndex(n, e.getYPosition());
        GridPane.setColumnIndex(n, e.getXPosition());
        GridPane.setColumnSpan(n, e.getWidth());
        GridPane.setRowSpan(n, e.getHeight());
        return n;
    }

    /**
     * Extracts the information from a GridPane for saving into a SavedTemplate
     * @param p GridPane containing data
     * @return DataMap
     */
    public static Map<String, String> getData(GridPane p) {
        Map<String, String> dataMap = new HashedMap();
        p.getChildren().forEach(node -> {
            if (!node.isDisabled()) {
                if (node instanceof TextField) {
                    dataMap.put(node.getId(), ((TextField) node).getText());
                } else if (node instanceof TextArea) {
                    dataMap.put(node.getId(), ((TextArea) node).getText());
                }
            }
        });
        return dataMap;
    }

}
