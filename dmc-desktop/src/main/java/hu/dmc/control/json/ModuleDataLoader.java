package hu.dmc.control.json;

import com.google.gson.reflect.TypeToken;
import hu.dmc.control.util.GsonFactory;
import hu.dmc.shared.model.generated.test.test.module.Module;
import hu.dmc.shared.model.generated.test.test.module.ModuleImpl;
import hu.dmc.shared.model.generated.test.test.saved_template.SavedTemplate;
import hu.dmc.shared.model.generated.test.test.saved_template.SavedTemplateImpl;
import org.apache.commons.io.FileUtils;
import hu.dmc.control.DatabaseHandler;
import hu.dmc.shared.model.generated.test.test.template.Template;
import hu.dmc.shared.model.generated.test.test.template.TemplateImpl;
import hu.dmc.shared.model.generated.test.test.template.generated.GeneratedTemplate;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ModuleDataLoader {

    /**
     * Loads up all the json files from the ./data directory.
     * Checks their validity and then tries to apply the changes in the templateSets
     *
     * @throws IOException
     */
    public static void loadDatas() throws IOException {
        String[] types = {"json"};
        File dir = new File("./data");
        if (!dir.exists() || !dir.isDirectory()) {
            dir.mkdir();
        }

        Iterator it = FileUtils.iterateFiles(dir, types, false);
        while (it.hasNext()) {
            String s = FileUtils.readFileToString(((File) it.next()), "UTF-8");
            ModuleData data = GsonFactory.getGson().fromJson(s, new TypeToken<ModuleData>() {
            }.getType());

            Optional<Module> optionalModule = DatabaseHandler.getModuleManager().stream()
                    .filter(Module.ID.equal(data.getModuleId())).findFirst();
            if (optionalModule.isPresent()) {
                Module ts = optionalModule.get();
                DefaultArtifactVersion currentVersion = new DefaultArtifactVersion(ts.getVersion());
                DefaultArtifactVersion jsonVersion = new DefaultArtifactVersion(data.getVersion());

                if (jsonVersion.compareTo(currentVersion) > 0) {
                    ts.setModuleName(data.getModuleName());
                    ts.setId(data.getModuleId());
                    ts.setVersion(data.getVersion());
                    ts = DatabaseHandler.getModuleManager().update(ts);

                    removeDeprecatedTemplates(data, ts);
                    createAndUpdateTemplates(data, ts);
                }
            } else {
                Module module = new ModuleImpl();
                module.setModuleName(data.getModuleName());
                module.setId(data.getModuleId());
                module.setVersion(data.getVersion());
                module = DatabaseHandler.getModuleManager().persist(module);

                for (ModuleTemplateData setTempData : data.getTemplates()) {
                    createAndSaveTemplate(setTempData, module.getId());
                }
            }
        }
    }

    /**
     * If the TemplateSet already exist, this method will find the Templates bound to it, and updates / creates them
     * according to the information stored in the ModuleData
     *
     * @param data the complete setData, which contains all the information of a TemplateSet
     * @param m    the existing TemplateSet object
     */
    private static void createAndUpdateTemplates(ModuleData data, Module m) {
        //Creating and updating new templates
        for (ModuleTemplateData moduleTemplateData : data.getTemplates()) {
            Optional<Template> t = DatabaseHandler.getTemplateManager().stream()
                    .filter(Template.NAME.equal(moduleTemplateData.getTemplateName()).and(Template.MODULE_ID.equal(data.getModuleId())))
                    .findFirst();
            if (t.isPresent()) {
                Template template = t.get();
                template.setName(moduleTemplateData.getTemplateName());
                template.setClientTemplate(moduleTemplateData.getClientTemplate());
                template.setVersioned(moduleTemplateData.getVersioned());
                template.setJsonString(GsonFactory.getGson().toJson(moduleTemplateData.getElementList()));

                DatabaseHandler.getTemplateManager().update(template);
                createDefaultDataIfAble(moduleTemplateData.getDefaultData(),m.getId(),moduleTemplateData.getTemplateName());
            } else {
                createAndSaveTemplate(moduleTemplateData, m.getId());
            }
        }
    }

    private static void createDefaultDataIfAble(List<ModuleTemplateDefaultData> defaultData, String moduleId, String templateName) {
        if(defaultData != null && !defaultData.isEmpty()){
            defaultData.forEach(defData -> {
                Optional<Template> temp = DatabaseHandler.getTemplateManager().stream()
                        .filter(Template.NAME.equal(templateName).and(Template.MODULE_ID.equal(moduleId)))
                        .findFirst();
                temp.ifPresent(t -> {
                    SavedTemplate st = new SavedTemplateImpl();
                    st.setData(GsonFactory.getGson().toJson(defData.getData()));
                    st.setName(defData.getName());
                    st.setTemplateId(t.getId());
                    st.setOriginal(true);
                    Optional<SavedTemplate> original = DatabaseHandler.getSavedTemplateManager().stream()
                            .filter(SavedTemplate.TEMPLATE_ID.equal(t.getId())
                                    .and(SavedTemplate.NAME.equal(defData.getName()))
                                    .and(SavedTemplate.ORIGINAL.equal(true)))
                            .findFirst();
                    if(original.isPresent()){
                        st.setId(original.get().getId());
                        DatabaseHandler.getSavedTemplateManager().update(st);
                    }else{
                        DatabaseHandler.getSavedTemplateManager().persist(st);
                    }
                });
            });
        }
    }

    /**
     * If Templates are removed from the ModuleData, earlier versions of said templates are deleted
     *
     * @param data the complete setData, which contains all the information of a TemplateSet
     * @param m    the existing TemplateSet object
     */
    private static void removeDeprecatedTemplates(ModuleData data, Module m) {
        List<String> oldNameList = DatabaseHandler.getTemplateManager().stream().filter(Template.MODULE_ID.equal(m.getId())).map(GeneratedTemplate::getName).collect(Collectors.toList());
        List<String> newNameList = data.getTemplates().stream().map(ModuleTemplateData::getTemplateName).collect(Collectors.toList());
        //Removing old templates
        oldNameList.forEach(s1 -> {
            if (!newNameList.contains(s1)) {
                Optional<Template> first = DatabaseHandler.getTemplateManager().stream()
                        .filter(Template.NAME.equal(s1).and(Template.MODULE_ID.equal(data.getModuleId())))
                        .findFirst();
                if (first.isPresent()) {
                    //Deleting old template's saves
                    DatabaseHandler.getSavedTemplateManager().stream()
                            .filter(SavedTemplate.TEMPLATE_ID.equal(first.get().getId()))
                            .forEach(DatabaseHandler.getSavedTemplateManager()::remove);
                    DatabaseHandler.getTemplateManager().remove(first.get());
                }
            }
        });
    }

    private static void createAndSaveTemplate(ModuleTemplateData setTempData, String templateSetId) {
        Template t = new TemplateImpl();
        t.setName(setTempData.getTemplateName());
        t.setModuleId(templateSetId);
        t.setClientTemplate(setTempData.getClientTemplate());
        t.setVersioned(setTempData.getVersioned());
        t.setJsonString(GsonFactory.getGson().toJson(setTempData.getElementList()));

        DatabaseHandler.getTemplateManager().persist(t);
        createDefaultDataIfAble(setTempData.getDefaultData(),templateSetId,setTempData.getTemplateName());
    }
}
