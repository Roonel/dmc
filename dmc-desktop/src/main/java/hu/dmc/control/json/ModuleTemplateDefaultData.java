package hu.dmc.control.json;

import lombok.Data;

import java.util.HashMap;

public @Data class ModuleTemplateDefaultData {
    private String name;
    private HashMap<String,String> data;
}
