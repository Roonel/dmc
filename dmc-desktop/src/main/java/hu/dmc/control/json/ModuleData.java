package hu.dmc.control.json;

import lombok.Data;

import java.util.List;

public @Data class ModuleData {
    private String moduleName;
    private String moduleId;
    private String version;
    private List<ModuleTemplateData> templates;
}