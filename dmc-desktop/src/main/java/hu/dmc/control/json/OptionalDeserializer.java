package hu.dmc.control.json;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.Optional;

public class OptionalDeserializer
            implements JsonSerializer<Optional> {
    @Override
    public JsonElement serialize(Optional optional, Type type, JsonSerializationContext jsonSerializationContext) {
        if(optional.isPresent()){
             return new JsonPrimitive(optional.get().toString());
        }else{
            return new JsonPrimitive("");
        }
    }
}
