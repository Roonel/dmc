package hu.dmc.control.json;

import hu.dmc.shared.model.Element;
import lombok.Data;

import java.util.HashMap;
import java.util.List;

public @Data class ModuleTemplateData {
    private String templateName;
    private Boolean clientTemplate;
    private Boolean versioned;
    private List<Element> elementList;
    private List<ModuleTemplateDefaultData> defaultData;
}