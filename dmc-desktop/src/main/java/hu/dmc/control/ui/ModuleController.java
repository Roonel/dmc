package hu.dmc.control.ui;

import hu.dmc.control.DatabaseHandler;
import hu.dmc.shared.model.generated.test.test.module.Module;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.LinkAction;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;

import javax.annotation.PostConstruct;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

@FXMLController(value = "/hu/dmc/view/Modules.fxml", title = "Dungeon Master Companion")
public class ModuleController extends AbstractController {

    @FXML
    private StackPane stackPane;
    @FXML
    @LinkAction(StartScreenController.class)
    private Button acceptBtn;
    @FXML
    private TableView<Module> moduleTable;
    @FXML
    private TableColumn<Module, String> moduleNameColumn;
    @FXML
    private TableColumn<Module, String> versionColumn;


    @PostConstruct
    public void init() {
        try {
            dataContext.setHostAddress(Inet4Address.getLocalHost().getHostAddress());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        acceptBtn.setDisable(true);

        moduleNameColumn.setCellValueFactory(p -> new SimpleStringProperty(p.getValue().getModuleName()));
        versionColumn.setCellValueFactory(p -> new SimpleStringProperty(p.getValue().getVersion()));

        List<Module> tSetList = DatabaseHandler.getModuleManager().stream().collect(Collectors.toList());
        ObservableList<Module> templateSets = FXCollections.observableArrayList();
        templateSets.addAll(tSetList);

        moduleTable.setItems(templateSets);
        moduleTable.setRowFactory(tv -> {
            TableRow<Module> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() > 1) {
                    try {
                        dataContext.setModule(row.getItem());
                        handler.navigate(StartScreenController.class);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            return row;
        });
        moduleTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            dataContext.setModule(newValue);
            acceptBtn.setDisable(newValue == null);
        });

        Platform.runLater(() -> {
            ResourceBundle resourceBundle = flowContext.getCurrentViewContext().getConfiguration().getResources();
            moduleTable.setPlaceholder(new Label(resourceBundle.getString("table.emptyLabel")));
        });
    }
}
