package hu.dmc.control.ui;

import hu.dmc.control.DatabaseHandler;
import hu.dmc.shared.model.generated.test.test.message.Message;
import hu.dmc.shared.model.generated.test.test.saved_template.SavedTemplate;
import hu.dmc.shared.model.generated.test.test.user.User;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.BackAction;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import lombok.Data;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@FXMLController(value = "/hu/dmc/view/Users.fxml", title = "Dungeon Master Companion")
public class UserController extends MenuScene {

    @BackAction
    @FXML
    private Button backBtn;

    @FXML
    private TableView<User> userTable;
    @FXML
    private TableColumn<User, String> nameColumn;
    @FXML
    private TableColumn<User, String> lastOnlineColumn;

    private ObservableList<User> oList;

    @PostConstruct
    public void init() {
        super.init();
        headerLabel.setText(resourceBundle.getString("label.users"));
        nameColumn.setCellValueFactory(p -> new SimpleStringProperty(p.getValue().getName()));
        lastOnlineColumn.setCellValueFactory(p -> new SimpleStringProperty(p.getValue().getLastOnline().toString()));
        userTable.setPlaceholder(new Label(resourceBundle.getString("table.emptyLabel")));
        userTable.setRowFactory(tv -> {
            TableRow<User> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                ContextMenu contextMenu = new ContextMenu();
                MenuItem deleteItem = new MenuItem(resourceBundle.getString("menu.delete"));
                deleteItem.setOnAction(e -> {
                    User user = row.getItem();
                    List<SavedTemplate> temps = DatabaseHandler.getSavedTemplateManager().stream()
                            .filter(SavedTemplate.OWNED_BY.equal(user.getId())).collect(Collectors.toList());
                    for (SavedTemplate temp : temps) {
                        temp.setOwnedBy(-1);
                        DatabaseHandler.getSavedTemplateManager().update(temp);
                    }
                    DatabaseHandler.getUserManager().remove(user);
                    refreshTable();
                });
                contextMenu.getItems().add(deleteItem);
                row.contextMenuProperty().bind(
                        Bindings.when(row.emptyProperty())
                                .then((ContextMenu) null)
                                .otherwise(contextMenu)
                );
            });
            return row;
        });

        oList = FXCollections.observableArrayList();
        userTable.setItems(oList);
        refreshTable();
    }

    private void refreshTable() {
        List<User> collect = DatabaseHandler.getUserManager().stream().collect(Collectors.toList());
        oList.clear();
        oList.addAll(collect);
    }
}
