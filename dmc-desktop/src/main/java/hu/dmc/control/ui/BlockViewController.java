package hu.dmc.control.ui;

import com.google.gson.reflect.TypeToken;
import hu.dmc.control.DatabaseHandler;
import hu.dmc.control.json.BlockUtil;
import hu.dmc.control.util.GsonFactory;
import hu.dmc.shared.model.Block;
import hu.dmc.shared.model.Element;
import hu.dmc.shared.model.generated.test.test.saved_template.SavedTemplate;
import hu.dmc.shared.model.generated.test.test.saved_template.generated.GeneratedSavedTemplate;
import hu.dmc.shared.model.generated.test.test.template.Template;
import hu.dmc.shared.model.generated.test.test.user.User;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.action.BackAction;
import io.datafx.controller.util.VetoException;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Orientation;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import org.apache.commons.collections.map.HashedMap;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.*;
import java.util.stream.Collectors;

@FXMLController(value = "/hu/dmc/view/BlockView.fxml", title = "Dungeon Master Companion")
public class BlockViewController extends MenuScene {

    @FXML
    private ListView<SavedTemplate> listView;
    private VBox dataBox;
    private ScrollPane scrollPane;
    private SplitPane innerSplit;
    @FXML
    private SplitPane outerSplitPane;
    @ActionTrigger("new")
    @FXML
    private Button newBlockBtn;

    @ActionTrigger("edit")
    @FXML
    private Button editBlockBtn;

    @BackAction
    @FXML
    private Button backBtn;

    private GridPane gridPane;
    private TableView<SavedTemplate> versionTable;

    private ObservableList<SavedTemplate> savedTemplates;
    private ObservableList<SavedTemplate> versionTemplates;
    private Template currentTemplate;

    @PostConstruct
    public void init() {
        super.init();
        dataBox = new VBox();

        scrollPane = new ScrollPane();
        scrollPane.setFitToWidth(true);
        scrollPane.setContent(dataBox);

        innerSplit = new SplitPane();
        innerSplit.setOrientation(Orientation.VERTICAL);
        if (dataContext.getVersionDividers() != null) {
            Platform.runLater(() ->
                    innerSplit.setDividerPositions(dataContext.getVersionDividers())
            );
        }
        if (dataContext.getViewDividers() != null) {
            Platform.runLater(() ->
                    outerSplitPane.setDividerPositions(dataContext.getViewDividers())
            );
        }

        versionTable = new TableView<>();
        versionTable.setPlaceholder(new Label(resourceBundle.getString("table.emptyLabel")));
        TableColumn<SavedTemplate, String> nameColumn = new TableColumn<>();
        nameColumn.setText(resourceBundle.getString("table.name"));
        nameColumn.setCellValueFactory(p -> new SimpleStringProperty(p.getValue().getName()));
        TableColumn<SavedTemplate, String> versionColumn = new TableColumn<>();
        versionColumn.setText(resourceBundle.getString("table.version"));
        versionColumn.setCellValueFactory(p -> new SimpleStringProperty(p.getValue().getVersion().toString()));
        TableColumn<SavedTemplate, String> userColumn = new TableColumn<>();
        userColumn.setText(resourceBundle.getString("table.modifiedBy"));
        userColumn.setCellValueFactory(param -> {
            if (param.getValue().getModifiedBy().isPresent()) {
                Optional<User> user = DatabaseHandler.getUserManager().stream()
                        .filter(User.ID.equal(param.getValue().getModifiedBy().getAsInt())).findFirst();
                if (user.isPresent()) {
                    return new SimpleStringProperty(user.get().getName());
                }else{
                    return new SimpleStringProperty(resourceBundle.getString("label.deletedUser"));
                }
            }
            return new SimpleStringProperty("DM");
        });
        versionTable.getColumns().addAll(nameColumn, versionColumn, userColumn);

        currentTemplate = DatabaseHandler.getTemplateManager().stream()
                .filter(Template.ID.equal(dataContext.getTemplateType()))
                .findFirst().get();
        headerLabel.setText(resourceBundle.getString("label.view") + currentTemplate.getName());

        listView.setCellFactory(lv -> {
            ListCell<SavedTemplate> cell = new ListCell<SavedTemplate>() {
                @Override
                protected void updateItem(SavedTemplate item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null) {
                        setText(item.getName());
                    } else {
                        setText("");
                    }
                }
            };
            ContextMenu contextMenu = new ContextMenu();
            MenuItem editItem = new MenuItem(resourceBundle.getString("menu.edit"));
            editItem.setOnAction(event -> {
                try {
                    edit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            MenuItem deleteItem = new MenuItem(resourceBundle.getString("menu.delete"));
            deleteItem.setOnAction(event -> {
                DatabaseHandler.getSavedTemplateManager().remove(cell.getItem());
                refreshItems();
                dataBox.getChildren().clear();
                outerSplitPane.getItems().set(1, scrollPane);
                if (dataContext.getViewDividers() != null) {
                    Platform.runLater(() ->
                            outerSplitPane.setDividerPositions(dataContext.getViewDividers())
                    );
                }
            });
            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    contextMenu.getItems().addAll(editItem, deleteItem);
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell;
        });

        listView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null && !newValue.equals(oldValue)) {
                dataBox.getChildren().clear();
                dataContext.setSavedTemplate(listView.getSelectionModel().getSelectedItem());
                dataContext.setViewDividers(outerSplitPane.getDividerPositions());

                if (currentTemplate.getVersioned()) {
                    refreshVersions();
                    dataContext.setVersionDividers(innerSplit.getDividerPositions());
                    innerSplit.getItems().clear();
                    innerSplit.getItems().add(scrollPane);
                    innerSplit.getItems().add(versionTable);
                    Platform.runLater(() -> {
                        innerSplit.setDividerPositions(dataContext.getVersionDividers());
                        versionTable.getSelectionModel().select(0);
                        outerSplitPane.getItems().set(1, innerSplit);
                        outerSplitPane.setDividerPositions(dataContext.getViewDividers());
                    });
                } else {
                    gridPane = getGridpane(listView.getSelectionModel().getSelectedItem());
                    dataBox.getChildren().add(gridPane);
                    outerSplitPane.getItems().set(1, scrollPane);
                    Platform.runLater(() ->
                            outerSplitPane.setDividerPositions(dataContext.getViewDividers())
                    );
                }
            }
        });
        versionTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null && !newValue.equals(oldValue)) {
                dataContext.setVersionDividers(innerSplit.getDividerPositions());

                gridPane = getGridpane(versionTable.getSelectionModel().getSelectedItem());
                dataBox.getChildren().clear();
                dataBox.getChildren().add(gridPane);
                innerSplit.getItems().set(0, scrollPane);

                Platform.runLater(() ->
                        innerSplit.setDividerPositions(dataContext.getVersionDividers())
                );
            }
        });

        savedTemplates = FXCollections.observableArrayList();
        versionTemplates = FXCollections.observableArrayList();
        refreshItems();
    }

    private GridPane getGridpane(SavedTemplate selectedItem) {
        Block block = new Block();
        String jsonString = currentTemplate.getJsonString();

        block.setElementList(GsonFactory.getGson().fromJson(jsonString, new TypeToken<List<Element>>() {
        }.getType()));
        block.setDataMap(GsonFactory.getGson().fromJson(selectedItem.getData().get(), new TypeToken<Map<String, String>>() {
        }.getType()));
        dataContext.setSavedTemplate(selectedItem);
        return BlockUtil.getGridPane(block, true);
    }

    private void refreshItems() {
        Collection<SavedTemplate> values = DatabaseHandler.getSavedTemplateManager().stream()
                .filter(SavedTemplate.TEMPLATE_ID.equal(dataContext.getTemplateType()))
                .collect(Collectors.toMap(GeneratedSavedTemplate::getId, temp -> temp, (u, u2) -> u))
                .values();
        savedTemplates.clear();
        savedTemplates.addAll(new ArrayList<>(values));
        listView.setItems(savedTemplates);
    }

    private void refreshVersions() {
        List<SavedTemplate> savedTemps = DatabaseHandler.getSavedTemplateManager().stream()
                .filter(SavedTemplate.ID.equal(dataContext.getSavedTemplate().getId()))
                .sorted((o1, o2) -> o2.getVersion().compareTo(o1.getVersion()))
                .collect(Collectors.toList());
        versionTemplates.clear();
        versionTemplates.addAll(savedTemps);
        versionTable.setItems(versionTemplates);
    }

    @ActionMethod("new")
    public void createNew() throws VetoException, FlowException {
        dataContext.setSavedTemplate(null);
        edit();
    }

    @ActionMethod("edit")
    public void edit() throws VetoException, FlowException {
        Block block = new Block();
        String jsonString = currentTemplate.getJsonString();
        block.setElementList(GsonFactory.getGson().fromJson(jsonString, new TypeToken<List<Element>>() {
        }.getType()));
        block.setDataMap(new HashedMap());
        dataContext.setSelectedBlock(block);
        handler.navigate(BlockEditorController.class);
    }

    @PreDestroy
    public void preDestroy() {
        dataContext.setViewDividers(outerSplitPane.getDividerPositions());
        dataContext.setVersionDividers(innerSplit.getDividerPositions());
    }

}
