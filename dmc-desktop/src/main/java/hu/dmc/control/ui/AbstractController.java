package hu.dmc.control.ui;


import hu.dmc.model.DataContext;
import io.datafx.controller.flow.context.ActionHandler;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.FlowActionHandler;
import io.datafx.controller.flow.context.ViewFlowContext;

import javax.inject.Inject;

public class AbstractController {

    @ActionHandler
    protected FlowActionHandler handler;

    @FXMLViewFlowContext
    protected ViewFlowContext flowContext;

    protected DataContext dataContext = DataContext.getInstance();

}
