package hu.dmc.control.ui;

import com.google.gson.reflect.TypeToken;
import hu.dmc.control.DatabaseHandler;
import hu.dmc.control.json.BlockUtil;
import hu.dmc.control.util.GsonFactory;
import hu.dmc.control.util.UserToStringConverter;
import hu.dmc.shared.model.Block;
import hu.dmc.shared.model.generated.test.test.saved_template.SavedTemplate;
import hu.dmc.shared.model.generated.test.test.saved_template.SavedTemplateImpl;
import hu.dmc.shared.model.generated.test.test.template.Template;
import hu.dmc.shared.model.generated.test.test.user.User;
import hu.dmc.shared.model.generated.test.test.user.UserImpl;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.util.VetoException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@FXMLController(value = "/hu/dmc/view/BlockEditor.fxml", title = "Dungeon Master Companion")
public class BlockEditorController extends MenuScene {

    @ActionTrigger("save")
    @FXML
    private Button saveButton;
    @ActionTrigger("backAction")
    @FXML
    private Button backButton;

    @FXML
    private TextField nameField;
    @FXML
    private ScrollPane dataPane;
    @FXML
    private VBox dataBox;
    @FXML
    private HBox restrictionBox;
    @FXML
    private ComboBox<User> ownedCombo;
    private GridPane gridPane;

    private User nobody;

    @PostConstruct
    public void init() {
        super.init();
        Block selectedBlock = dataContext.getSelectedBlock();
        ownedCombo.setCellFactory(new Callback<ListView<User>, ListCell<User>>() {
            @Override
            public ListCell<User> call(ListView<User> param) {
                return new ListCell<User>() {
                    @Override
                    protected void updateItem(User item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            setText(item.getName());
                        } else {
                            setText(resourceBundle.getString("label.onEmptyRestriction"));
                        }
                    }
                };
            }
        });
        ownedCombo.setConverter(new UserToStringConverter(resourceBundle.getString("label.onEmptyRestriction")));

        Template currentTemplate = DatabaseHandler.getTemplateManager().stream()
                .filter(Template.ID.equal(dataContext.getTemplateType()))
                .findFirst().get();

        if (currentTemplate.getClientTemplate()) {
            ObservableList<User> oList = FXCollections.observableArrayList();
            ownedCombo.setItems(oList);
            oList.add(null);
            oList.addAll(DatabaseHandler.getUserManager().stream().collect(Collectors.toList()));
            nobody = new UserImpl();
            nobody.setId(-1);
            nobody.setName(resourceBundle.getString("label.nobody"));
            oList.add(nobody);
        } else {
            dataBox.getChildren().remove(restrictionBox);
        }


        if (dataContext.getSavedTemplate() != null) {
            selectedBlock.setDataMap(GsonFactory.getGson().fromJson(dataContext.getSavedTemplate().getData().get(), new TypeToken<Map<String, String>>() {
            }.getType()));
            nameField.setText(dataContext.getSavedTemplate().getName());
            if (dataContext.getSavedTemplate().getOwnedBy().isPresent()) {
                Optional<User> u = DatabaseHandler.getUserManager().stream().filter(User.ID.equal(dataContext.getSavedTemplate().getOwnedBy().getAsInt())).findFirst();
                if(u.isPresent()){
                    ownedCombo.getSelectionModel().select(u.get());
                }else{
                    ownedCombo.getSelectionModel().select(nobody);
                }
            }
        }
        gridPane = BlockUtil.getGridPane(selectedBlock, false);
        dataPane.setContent(gridPane);
        headerLabel.setText(resourceBundle.getString("label.underEdit") + nameField.getText());
    }

    @ActionMethod("save")
    public void save() throws VetoException, FlowException {
        Map<String, String> data = BlockUtil.getData(gridPane);
        SavedTemplate sv = dataContext.getSavedTemplate();
        boolean persist = false;
        if (sv == null) {
            sv = new SavedTemplateImpl();
            sv.setTemplateId(dataContext.getTemplateType());
            persist = true;
            //Have to save new record, but with same id and new version timestamp
        } else {
            Template template = DatabaseHandler.getTemplateManager().stream()
                    .filter(Template.ID.equal(sv.getTemplateId()).and(Template.MODULE_ID.equal(dataContext.getModule().getId())))
                    .findFirst().get();

            if (template.getVersioned()) {
                sv.setTemplateId(dataContext.getTemplateType());
                sv.setId(sv.getId());
                sv.setVersion(Timestamp.valueOf(LocalDateTime.now()));
                persist = true;
            }
        }

        sv.setName(nameField.getText());
        User owner = ownedCombo.getSelectionModel().getSelectedItem();
        sv.setOwnedBy(owner == null ? null : owner.getId());
        sv.setData(GsonFactory.getGson().toJson(data));
        sv.setModifiedBy(null);
        sv.setOriginal(false);
        sv.setUpdated(Timestamp.valueOf(LocalDateTime.now()));
        if (persist) {
            DatabaseHandler.getSavedTemplateManager().persist(sv);
        } else {
            DatabaseHandler.getSavedTemplateManager().update(sv);
        }
        handler.navigateBack();
    }
}
