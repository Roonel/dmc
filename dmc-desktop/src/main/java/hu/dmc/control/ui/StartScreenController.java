package hu.dmc.control.ui;

import io.datafx.controller.FXMLController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import hu.dmc.control.DatabaseHandler;
import hu.dmc.shared.model.generated.test.test.template.Template;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@FXMLController(value = "/hu/dmc/view/StartScreen.fxml", title = "Dungeon Master Companion")
public class StartScreenController extends MenuScene{

    @FXML
    private VBox pane;
    @FXML
    private Label moduleLbl;

    @PostConstruct
    public void init() {
        super.init();
        headerLabel.setText(dataContext.getModule().getModuleName());
    }
}
