package hu.dmc.control.ui;

import hu.dmc.control.DatabaseHandler;
import hu.dmc.shared.model.generated.test.test.template.Template;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class MenuScene extends AbstractController {

    @FXML
    protected Label headerLabel;
    @FXML
    private Label ipLabel;
    @FXML
    private ToolBar menuToolbar;
    protected ResourceBundle resourceBundle;

    @PostConstruct
    public void init() {
        resourceBundle = flowContext.getCurrentViewContext().getConfiguration().getResources();
        ipLabel.setText(dataContext.getHostAddress());
        List<Template> templateList = DatabaseHandler.getTemplateManager().stream()
                .filter(Template.MODULE_ID.equal(dataContext.getModule().getId()))
                .collect(Collectors.toList());
        Button msgBtn = new Button(resourceBundle.getString("menu.messages"));
        msgBtn.setOnAction(event -> {
            try {
                handler.navigate(MessageController.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        Button userBrn = new Button(resourceBundle.getString("menu.users"));
        userBrn.setOnAction(event -> {
            try {
                handler.navigate(UserController.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        menuToolbar.getItems().add(userBrn);
        menuToolbar.getItems().add(msgBtn);
        templateList.forEach(template -> {
            Button b = new Button(template.getName());
            b.setOnAction(event -> {
                dataContext.setTemplateType(template.getId());
                try {
                    handler.navigate(BlockViewController.class);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            menuToolbar.getItems().add(b);
        });

    }
}
