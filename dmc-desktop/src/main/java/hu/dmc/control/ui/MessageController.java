package hu.dmc.control.ui;

import hu.dmc.control.DatabaseHandler;
import hu.dmc.control.util.DragAndDropPane;
import hu.dmc.shared.model.generated.test.test.message.Message;
import hu.dmc.shared.model.generated.test.test.message.MessageImpl;
import hu.dmc.shared.model.generated.test.test.user.User;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.action.BackAction;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@FXMLController(value = "/hu/dmc/view/Messages.fxml", title = "Dungeon Master Companion")
public class MessageController extends MenuScene {

    @BackAction
    @FXML
    private Button backBtn;

    @ActionTrigger("newMessage")
    @FXML
    private Button newMsgBtn;

    @FXML
    private TableView<Message> msgTable;
    @FXML
    private TableColumn<Message, String> titleColumn;
    @FXML
    private TableColumn<Message, String> msgColumn;
    @FXML
    private TableColumn<Message, String> recipientColumn;
    @FXML
    private TableColumn<Message, String> sentColumn;
    @FXML
    private TableColumn<Message, String> hasImg;

    private ObservableList<Message> oList;

    @PostConstruct
    public void init() {
        super.init();
        headerLabel.setText(resourceBundle.getString("label.messages"));
        titleColumn.setCellValueFactory(p -> new SimpleStringProperty(p.getValue().getTitle()));
        msgColumn.setCellValueFactory(p -> {
            if (p.getValue().getMessage().isPresent()) {
                return new SimpleStringProperty(p.getValue().getMessage().get());
            } else {
                return new SimpleStringProperty("");
            }
        });
        recipientColumn.setCellValueFactory(p -> {
            if (p.getValue().getRecipient().isPresent()) {
                Optional<User> user = DatabaseHandler.getUserManager().stream().filter(User.ID.equal(p.getValue().getRecipient().getAsInt())).findFirst();
                if (user.isPresent()) {
                    return new SimpleStringProperty(user.get().getName());
                }else{
                    return new SimpleStringProperty(resourceBundle.getString("label.deletedUser"));
                }
            }
            return new SimpleStringProperty("");
        });
        sentColumn.setCellValueFactory(p -> new SimpleStringProperty(p.getValue().getSent().toString()));
        hasImg.setCellValueFactory(p -> new SimpleStringProperty(p.getValue().getImgData().isPresent() ? resourceBundle.getString("label.yes") : resourceBundle.getString("label.no")));
        msgTable.setPlaceholder(new Label(resourceBundle.getString("table.emptyLabel")));
        msgTable.setRowFactory(tv -> {
            TableRow<Message> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                final ContextMenu contextMenu = new ContextMenu();
                final MenuItem removeMenuItem = new MenuItem(resourceBundle.getString("menu.remove"));
                final MenuItem examineMenuItem = new MenuItem(resourceBundle.getString("menu.examine"));
                removeMenuItem.setOnAction(event1 -> {
                    DatabaseHandler.getMessageManager().remove(row.getItem());
                    refreshTable();
                });
                examineMenuItem.setOnAction(event1 -> {
                    openMessageModal(row.getItem());
                });

                contextMenu.getItems().add(examineMenuItem);
                contextMenu.getItems().add(removeMenuItem);
                row.contextMenuProperty().bind(
                        Bindings.when(row.emptyProperty())
                                .then((ContextMenu) null)
                                .otherwise(contextMenu)
                );

                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    Message rowData = row.getItem();
                    openMessageModal(rowData);
                }
            });
            return row;
        });

        oList = FXCollections.observableArrayList();
        msgTable.setItems(oList);
        refreshTable();
    }

    private void refreshTable() {
        List<Message> collect = DatabaseHandler.getMessageManager().stream().collect(Collectors.toList());
        oList.clear();
        oList.addAll(collect);
    }

    @ActionMethod("newMessage")
    public void newMessage() {
        openMessageModal(null);
    }

    private void openMessageModal(Message m) {
        Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(backBtn.getParent().getScene().getWindow());
        VBox dialogVbox = new VBox(20);
        dialogVbox.setPadding(new Insets(15));

        Label recLbl = new Label(resourceBundle.getString("label.recipient"));
        FlowPane userList = new FlowPane();
        userList.setHgap(5);
        userList.setVgap(5);
        DatabaseHandler.getUserManager().stream().forEach(u -> {
            CheckBox cb = new CheckBox();
            cb.setText(u.getName());
            cb.setUserData(u);
            userList.getChildren().add(cb);
        });
        Label titleLbl = new Label(resourceBundle.getString("label.title"));
        TextField titleField = new TextField();
        titleField.setEditable(m == null);
        Label msgLbl = new Label(resourceBundle.getString("label.message"));
        TextArea area = new TextArea();
        area.setMinHeight(60);
        area.setEditable(m == null);

        DragAndDropPane ddp = new DragAndDropPane(resourceBundle.getString("label.imageDrag"));
        HBox containerHBox = new HBox();
        containerHBox.setAlignment(Pos.CENTER);
        ScrollPane imgScrollPane = new ScrollPane();
        imgScrollPane.setPrefSize(620, 620);
        imgScrollPane.setContent(ddp);
        containerHBox.getChildren().add(imgScrollPane);
        ddp.setEditable(m == null);

        Button sendBtn = new Button(resourceBundle.getString("button.sendMsg"));
        sendBtn.setOnAction(event -> {
            int count = 0;
            for (Node node : userList.getChildren()) {
                if (node instanceof CheckBox) {
                    CheckBox c = (CheckBox) node;
                    if (c.isSelected()) {
                        sendMsg(titleField.getText(), area.getText(), ddp, ((User) c.getUserData()).getId());
                        count++;
                    }
                }
            }
            if (count == 0) {
                sendMsg(titleField.getText(), area.getText(), ddp, null);
            }
            dialog.close();
            refreshTable();
        });

        Button closeBtn = new Button(resourceBundle.getString("button.close"));
        closeBtn.setOnAction(e -> dialog.close());

        HBox buttonBox = new HBox(40);
        if (m == null) {
            buttonBox.getChildren().add(sendBtn);
        }
        buttonBox.getChildren().add(closeBtn);

        if (m != null) {
            if (m.getRecipient().isPresent()) {
                Optional<User> user = DatabaseHandler.getUserManager().stream().filter(User.ID.equal(m.getRecipient().getAsInt())).findFirst();
                if(user.isPresent()){
                    recLbl.setText(resourceBundle.getString("label.recipient") + user.get().getName());
                }else{
                    recLbl.setText(resourceBundle.getString("label.recipient") + resourceBundle.getString("label.deletedUser"));
                }
            } else {
                recLbl.setText(resourceBundle.getString("label.recipientAll"));
            }
            Label dateLbl = new Label(resourceBundle.getString("label.sent") + m.getSent().toString());
            titleField.setText(m.getTitle());
            if (m.getMessage().isPresent()) {
                area.setText(m.getMessage().get());
            }
            if (m.getImgData().isPresent()) {
                ddp.addImage(new Image(new ByteArrayInputStream(Base64.getDecoder().decode(m.getImgData().get()))));
            }
            dialogVbox.getChildren().addAll(recLbl, dateLbl, titleLbl, titleField, msgLbl, area, containerHBox, buttonBox);
        } else {
            dialogVbox.getChildren().addAll(recLbl, userList, titleLbl, titleField, msgLbl, area, containerHBox, buttonBox);
        }

        Scene dialogScene = new Scene(dialogVbox, 800, 600);
        dialog.setScene(dialogScene);
        dialog.show();
    }

    private void sendMsg(String title, String msgString, DragAndDropPane ddp, Integer recipient) {
        Message msg = new MessageImpl();
        msg.setTitle(title);
        msg.setMessage(msgString);
        msg.setRecipient(recipient);
        if (ddp.getImage() != null) {
            msg.setImgData(Base64.getEncoder().encodeToString(ddp.getImageBytes()));
        }
        DatabaseHandler.getMessageManager().persist(msg);
    }

}
