package hu.dmc.control.rest;

import hu.dmc.control.DatabaseHandler;
import hu.dmc.control.util.GsonFactory;
import hu.dmc.model.DataContext;
import hu.dmc.shared.model.generated.test.test.user.User;
import hu.dmc.shared.model.generated.test.test.user.UserImpl;
import hu.dmc.shared.rest.RestData;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Optional;

@Path("/")
@Produces("application/json")
@Consumes("application/json")
public class LoginService {

    @GET
    public Response login(@QueryParam("user") String userName, @QueryParam("password") String password){
        Optional<User> user = DatabaseHandler.getUserManager().stream().filter(User.NAME.equal(userName)).findFirst();
        if(user.isPresent()){
            if(user.get().getPassword().equals(password)){
                String s = GsonFactory.getGson().toJson(user.get());
                return Response.ok(new RestData(s)).build();
            }else{
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
        }else{
            User u = new UserImpl();
            u.setName(userName);
            u.setPassword(password);
            u = DatabaseHandler.getUserManager().persist(u);
            String s = GsonFactory.getGson().toJson(u);
            return Response.ok(new RestData(s)).build();
        }
    }
}
