package hu.dmc.control.rest;

import io.undertow.Undertow;
import org.jboss.resteasy.plugins.server.undertow.UndertowJaxrsServer;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

public class RestHandler {
    private static UndertowJaxrsServer server;

    @ApplicationPath("/")
    public static class RestApp extends Application {
        @Override
        public Set<Class<?>> getClasses() {
            HashSet<Class<?>> classes = new HashSet<>();
            classes.add(LoginService.class);
            classes.add(DataService.class);
            return classes;
        }
    }

    public static void startServer() {
        server = new UndertowJaxrsServer();
        Undertow.Builder serverBuilder = Undertow.builder().addHttpListener(8080, "0.0.0.0");
        server.start(serverBuilder);
        server.deploy(RestApp.class);
    }

    public static void stopServer() {
        if (server != null) {
            server.stop();
        }
    }
}
