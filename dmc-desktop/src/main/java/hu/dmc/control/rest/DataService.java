package hu.dmc.control.rest;

import com.google.gson.reflect.TypeToken;
import hu.dmc.control.DatabaseHandler;
import hu.dmc.control.util.GsonFactory;
import hu.dmc.model.DataContext;
import hu.dmc.shared.model.generated.test.test.message.Message;
import hu.dmc.shared.model.generated.test.test.saved_template.SavedTemplate;
import hu.dmc.shared.model.generated.test.test.saved_template.SavedTemplateImpl;
import hu.dmc.shared.model.generated.test.test.saved_template.generated.GeneratedSavedTemplate;
import hu.dmc.shared.model.generated.test.test.saved_template.generated.GeneratedSavedTemplateImpl;
import hu.dmc.shared.model.generated.test.test.template.Template;
import hu.dmc.shared.model.generated.test.test.user.User;
import hu.dmc.shared.rest.RefreshData;
import hu.dmc.shared.rest.RestData;
import hu.dmc.shared.rest.TemplateData;
import io.datafx.controller.injection.scopes.FlowScoped;

import javax.annotation.PreDestroy;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Path("/data/")
@Produces("application/json")
@Consumes("application/json")
@FlowScoped
public class DataService {

    private DataContext dataContext = DataContext.getInstance();

    @GET
    @Path("/templates")
    public Response getTemplates(@QueryParam("user") String userId) {
        List<Template> templateList = DatabaseHandler.getTemplateManager().stream()
                .filter(Template.MODULE_ID.equal(dataContext.getModule().getId())
                        .and(Template.CLIENT_TEMPLATE.equal(true)))
                .collect(Collectors.toList());

        if (templateList.isEmpty()) {
            return Response.status(Response.Status.NO_CONTENT).build();
        }

        Collection<SavedTemplate> values = DatabaseHandler.getSavedTemplateManager().stream()
                .filter(SavedTemplate.TEMPLATE_ID.in(templateList.stream().map(Template::getId).collect(Collectors.toSet()))
                        .and(SavedTemplate.OWNED_BY.equal(Integer.valueOf(userId))
                                .or(SavedTemplate.OWNED_BY.isNull())))
                .sorted((o1, o2) -> o2.getVersion().compareTo(o1.getVersion()))
                .collect(Collectors.toMap(GeneratedSavedTemplate::getId, temp -> temp, (u, u2) -> u))
                .values();

        TemplateData td = new TemplateData();
        td.setTemplateList(templateList);
        td.setSavedTemplateList(new ArrayList<>(values));
        String s = GsonFactory.getGson().toJson(td);
        return Response.ok(new RestData(s)).build();
    }

    @GET
    @Path("/saveTemplate")
    public Response saveTemplate(@QueryParam("user") String userId, @QueryParam("templateData") String templateJson) {
        SavedTemplate savedTemp = GsonFactory.getGson().fromJson(templateJson, new TypeToken<SavedTemplateImpl>() {}.getType());
        savedTemp.setModifiedBy(Integer.valueOf(userId));
        savedTemp.setVersion(Timestamp.valueOf(LocalDateTime.now()));
        savedTemp.setOriginal(false);
        DatabaseHandler.getSavedTemplateManager().persist(savedTemp);

        return Response.status(Response.Status.ACCEPTED).build();
    }

    @GET
    @Path("/refresh")
    public Response getMessages(@QueryParam("user") String userId, @QueryParam("after") String after) {
        Stream<Message> messageStream = DatabaseHandler.getMessageManager().stream()
                .filter(Message.RECIPIENT.equal(Integer.valueOf(userId))
                        .or(Message.RECIPIENT.isNull()));
        if(after != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime dateTime = LocalDateTime.parse(after, formatter);
            messageStream = messageStream.filter(Message.SENT.greaterThan(Timestamp.valueOf(dateTime)));
        }
        List<Message> msgList = messageStream.collect(Collectors.toList());

        List<Template> templateList = DatabaseHandler.getTemplateManager().stream()
                .filter(Template.MODULE_ID.equal(dataContext.getModule().getId())
                        .and(Template.CLIENT_TEMPLATE.equal(true)))
                .collect(Collectors.toList());

        Stream<SavedTemplate> savedTemplateStream = DatabaseHandler.getSavedTemplateManager().stream()
                .filter(SavedTemplate.TEMPLATE_ID.in(templateList.stream().map(Template::getId).collect(Collectors.toSet()))
                        .and(SavedTemplate.OWNED_BY.equal(Integer.valueOf(userId))
                                .or(SavedTemplate.OWNED_BY.isNull())));
        if(after != null){
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime dateTime = LocalDateTime.parse(after, formatter);
            savedTemplateStream = savedTemplateStream.filter(SavedTemplate.UPDATED.greaterThan(Timestamp.valueOf(dateTime)));
        }

        Collection<SavedTemplate> savedTemplates = savedTemplateStream.sorted((o1, o2) -> o2.getVersion().compareTo(o1.getVersion()))
                .collect(Collectors.toMap(GeneratedSavedTemplate::getId, temp -> temp, (u, u2) -> u))
                .values();


        Optional<User> user = DatabaseHandler.getUserManager().stream().filter(User.ID.equal(Integer.valueOf(userId))).findFirst();
        user.get().setLastOnline(Timestamp.valueOf(LocalDateTime.now()));
        DatabaseHandler.getUserManager().update(user.get());

        RefreshData rd = new RefreshData();
        rd.setMessages(msgList);
        rd.setSavedTemplateList(new ArrayList<>(savedTemplates));

        String s = GsonFactory.getGson().toJson(rd);
        return Response.ok(new RestData(s)).build();
    }

}
