package hu.dmc.control;

import ch.vorburger.exec.ManagedProcessException;
import ch.vorburger.mariadb4j.DB;
import ch.vorburger.mariadb4j.DBConfigurationBuilder;
import hu.dmc.shared.model.generated.SharedApplication;
import hu.dmc.shared.model.generated.SharedApplicationBuilder;
import hu.dmc.shared.model.generated.test.test.message.MessageManager;
import hu.dmc.shared.model.generated.test.test.module.ModuleManager;
import hu.dmc.shared.model.generated.test.test.saved_template.SavedTemplateManager;
import hu.dmc.shared.model.generated.test.test.template.TemplateManager;
import hu.dmc.shared.model.generated.test.test.user.UserManager;
import lombok.Getter;

import java.sql.*;
import java.util.Scanner;

public class DatabaseHandler {
    private static DB db;
    private static DBConfigurationBuilder configBuilder;
    private static SharedApplication app;
    @Getter
    private static ModuleManager moduleManager;
    @Getter
    private static TemplateManager templateManager;
    @Getter
    private static SavedTemplateManager savedTemplateManager;
    @Getter
    private static MessageManager messageManager;
    @Getter
    private static UserManager userManager;


    /**
     * Starts the built-in database with the default configuration
     *
     * @throws ManagedProcessException
     */
    public static void startDb() throws ManagedProcessException {
        configBuilder = DBConfigurationBuilder.newBuilder();
        configBuilder.setPort(3305); // OR, default: setPort(0); => autom. detect free port
        configBuilder.setDataDir("./db/");
        db = DB.newEmbeddedDB(configBuilder.build());

        db.start();

        loadSqls();

        app = new SharedApplicationBuilder().build();
        moduleManager = app.getOrThrow(ModuleManager.class);
        templateManager = app.getOrThrow(TemplateManager.class);
        savedTemplateManager = app.getOrThrow(SavedTemplateManager.class);
        messageManager = app.getOrThrow(MessageManager.class);
        userManager = app.getOrThrow(UserManager.class);
    }

    private static void loadSqls() {
        try {
            Connection conn = getConnection();
            conn.setAutoCommit(false);
            Scanner sc = new Scanner(DatabaseHandler.class.getResourceAsStream("/db/import.sql"), "UTF-8");
            while(sc.useDelimiter(";").hasNext()){
                try {
                    Statement statement = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
                    String q = sc.useDelimiter(";").next();
                    statement.executeUpdate(q);
                    statement.close();
                }catch (Exception e){}
            }
            sc.close();
            conn.commit();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(configBuilder.getURL("test"), "root", "");
    }


    /**
     * Stops the built-in database.
     *
     * @throws ManagedProcessException
     */
    public static void stopDb() throws ManagedProcessException {
        if (db != null) {
            db.stop();
        }
    }
}
