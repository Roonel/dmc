package hu.dmc.control.util;

import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class DragAndDropPane extends StackPane {

    private ImageView imageView;
    private File currentFile;
    private boolean editable = true;

    public DragAndDropPane(String text) {
        setOnDragOver(this::mouseDragOver);
        setOnDragDropped(this::mouseDragDropped);
        setOnDragExited(event -> setStyle("-fx-border-color: #6a6a6a;"));
        setPrefHeight(600);
        setPrefWidth(600);
        getChildren().add(new Text(text));
    }

    public void addImage(Image i){
        imageView = new ImageView();
        imageView.setImage(i);
        imageView.setPreserveRatio(true);
        imageView.setFitWidth(600);
        getChildren().clear();
        getChildren().add(imageView);
    }

    private void mouseDragDropped(final DragEvent e) {
        if(!editable) {
            return;
        }
        final Dragboard db = e.getDragboard();
        boolean success = false;
        if (db.hasFiles()) {
            success = true;
            // Only get the first file from the list
            final File file = db.getFiles().get(0);
            currentFile = file;
            Platform.runLater(() -> {
                try {
                    if(!getChildren().isEmpty()){
                        getChildren().remove(0);
                    }
                    Image img = new Image(new FileInputStream(file.getAbsolutePath()));

                    addImage(img);
                } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
                }
            });
        }
        e.setDropCompleted(success);
        e.consume();
    }

    private  void mouseDragOver(final DragEvent e) {
        if(!editable) {
            return;
        }
        final Dragboard db = e.getDragboard();

        final boolean isAccepted = db.getFiles().get(0).getName().toLowerCase().endsWith(".png")
                || db.getFiles().get(0).getName().toLowerCase().endsWith(".jpeg")
                || db.getFiles().get(0).getName().toLowerCase().endsWith(".jpg");

        if (db.hasFiles()) {
            if (isAccepted) {
                setStyle("-fx-border-color: red;"
                        + "-fx-border-width: 5;"
                        + "-fx-background-color: #C6C6C6;"
                        + "-fx-border-style: solid;");
                e.acceptTransferModes(TransferMode.COPY);
            }
        } else {
            e.consume();
        }
    }

    public Image getImage() {
        if(imageView != null){
            return imageView.getImage();
        }else{
            return null;
        }
    }

    public byte[] getImageBytes() {
        if(currentFile == null){
            return null;
        }
        try {
            return IOUtils.toByteArray(new FileInputStream(currentFile));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }
}