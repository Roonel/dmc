package hu.dmc.control.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import hu.dmc.control.json.OptionalDeserializer;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Generates a GSON specifically designed for compatibility  with the Speedment api
 */
public class GsonFactory {
    private static GsonBuilder gsonBuilder = new GsonBuilder();
    static{
        gsonBuilder.registerTypeAdapter(Optional.class, new OptionalDeserializer());
    }

    public static Gson getGson(){
        return gsonBuilder.create();
    }
}
