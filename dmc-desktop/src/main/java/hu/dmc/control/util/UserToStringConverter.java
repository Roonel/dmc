package hu.dmc.control.util;

import hu.dmc.shared.model.generated.test.test.user.User;
import javafx.util.StringConverter;

public class UserToStringConverter extends StringConverter<User> {

    private String onEmptyText;

    public UserToStringConverter(String onEmptyText) {
        super();
        this.onEmptyText = onEmptyText;
    }

    @Override
    public String toString(User object) {
        if(object != null){
            return object.getName();
        }
        return onEmptyText;
    }

    @Override
    public User fromString(String string) {
        return null;
    }
}