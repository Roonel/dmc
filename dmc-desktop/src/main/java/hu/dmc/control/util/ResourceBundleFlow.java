package hu.dmc.control.util;

import io.datafx.controller.ViewConfiguration;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.context.ViewFlowContext;

public class ResourceBundleFlow extends Flow {

    public ResourceBundleFlow(Class<?> startViewControllerClass, ViewConfiguration viewConfiguration) {
        super(startViewControllerClass, viewConfiguration);
    }

    @Override
    public FlowHandler createHandler(ViewFlowContext flowContext) {
        return new FlowHandler(this, flowContext, this.getViewConfiguration());
    }

}