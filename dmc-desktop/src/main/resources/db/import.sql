CREATE TABLE test.TEMPLATE (
  id              MEDIUMINT NOT NULL AUTO_INCREMENT,
  name            CHAR(30) NOT NULL,
  json_string     TEXT NOT NULL,
  module_id       CHAR(30) NOT NULL,
  client_template MEDIUMINT NOT NULL DEFAULT 0,
  versioned      MEDIUMINT NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
);

CREATE TABLE test.SAVED_TEMPLATE (
  id          MEDIUMINT NOT NULL AUTO_INCREMENT,
  name        CHAR(255) NOT NULL,
  template_id MEDIUMINT NOT NULL,
  data        TEXT,
  ownedBy     MEDIUMINT, #can be null, it means that everyone has access to it
  original    MEDIUMINT NOT NULL DEFAULT 0,
  version     TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated     TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  modified_by MEDIUMINT,
  PRIMARY KEY (id, version)
);

CREATE TABLE test.MODULE (
  id                  CHAR(30) NOT NULL,
  module_name         CHAR(255) NOT NULL,
  version             CHAR(30) DEFAULT 0 NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE test.MESSAGE (
  id        MEDIUMINT NOT NULL AUTO_INCREMENT,
  title     CHAR(255) NOT NULL,
  message   TEXT,
  img_data  LONGTEXT,
  sent      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  recipient MEDIUMINT,
  PRIMARY KEY (ID)
);

CREATE TABLE test.USER(
  id  MEDIUMINT NOT NULL AUTO_INCREMENT,
  name  char(255) not NULL UNIQUE,
  password text not null,
  last_online TIMESTAMP DEFAULT current_timestamp,
  PRIMARY KEY (id)
);