package hu.dmc.shared.rest;

import hu.dmc.shared.model.generated.test.test.message.Message;
import hu.dmc.shared.model.generated.test.test.saved_template.SavedTemplate;
import lombok.Data;

import java.util.List;

public @Data class RefreshData {
    private List<Message> messages;
    private List<SavedTemplate> savedTemplateList;
}
