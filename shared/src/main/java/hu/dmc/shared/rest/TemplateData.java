package hu.dmc.shared.rest;

import hu.dmc.shared.model.generated.test.test.saved_template.SavedTemplate;
import hu.dmc.shared.model.generated.test.test.template.Template;
import lombok.Data;

import java.util.List;

public @Data class TemplateData {
    private List<Template> templateList;
    private List<SavedTemplate> savedTemplateList;
}
