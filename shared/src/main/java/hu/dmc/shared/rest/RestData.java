package hu.dmc.shared.rest;


import lombok.Data;

/**
 * Simple object for handling http responses
 */
public @Data class RestData {

    /**
     * The actual data from the response
     */
    private Object data;
    public RestData(Object data) {
        this.data = data;
    }
}
