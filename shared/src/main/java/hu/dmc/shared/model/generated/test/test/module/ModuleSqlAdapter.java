package hu.dmc.shared.model.generated.test.test.module;

import hu.dmc.shared.model.generated.test.test.module.generated.GeneratedModuleSqlAdapter;

/**
 * The SqlAdapter for every {@link
 * hu.dmc.shared.model.generated.test.test.module.Module} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public class ModuleSqlAdapter extends GeneratedModuleSqlAdapter {
    
    
}