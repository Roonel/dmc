package hu.dmc.shared.model.generated.test.test.user;

import hu.dmc.shared.model.generated.test.test.user.generated.GeneratedUserSqlAdapter;

/**
 * The SqlAdapter for every {@link
 * hu.dmc.shared.model.generated.test.test.user.User} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public class UserSqlAdapter extends GeneratedUserSqlAdapter {
    
    
}