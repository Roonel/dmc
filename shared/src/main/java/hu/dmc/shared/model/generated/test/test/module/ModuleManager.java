package hu.dmc.shared.model.generated.test.test.module;

import hu.dmc.shared.model.generated.test.test.module.generated.GeneratedModuleManager;

/**
 * The main interface for the manager of every {@link
 * hu.dmc.shared.model.generated.test.test.module.Module} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public interface ModuleManager extends GeneratedModuleManager {
    
    
}