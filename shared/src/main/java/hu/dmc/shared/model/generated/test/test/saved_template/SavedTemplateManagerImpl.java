package hu.dmc.shared.model.generated.test.test.saved_template;

import hu.dmc.shared.model.generated.test.test.saved_template.generated.GeneratedSavedTemplateManagerImpl;

/**
 * The default implementation for the manager of every {@link
 * hu.dmc.shared.model.generated.test.test.saved_template.SavedTemplate} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public final class SavedTemplateManagerImpl extends GeneratedSavedTemplateManagerImpl implements SavedTemplateManager {
    
    
}