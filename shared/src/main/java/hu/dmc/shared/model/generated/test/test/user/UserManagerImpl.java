package hu.dmc.shared.model.generated.test.test.user;

import hu.dmc.shared.model.generated.test.test.user.generated.GeneratedUserManagerImpl;

/**
 * The default implementation for the manager of every {@link
 * hu.dmc.shared.model.generated.test.test.user.User} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public final class UserManagerImpl extends GeneratedUserManagerImpl implements UserManager {
    
    
}