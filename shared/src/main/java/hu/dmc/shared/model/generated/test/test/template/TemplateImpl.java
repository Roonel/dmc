package hu.dmc.shared.model.generated.test.test.template;

import hu.dmc.shared.model.generated.test.test.template.generated.GeneratedTemplateImpl;

/**
 * The default implementation of the {@link
 * hu.dmc.shared.model.generated.test.test.template.Template}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public final class TemplateImpl extends GeneratedTemplateImpl implements Template {
    
    
}