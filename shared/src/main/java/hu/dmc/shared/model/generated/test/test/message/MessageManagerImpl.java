package hu.dmc.shared.model.generated.test.test.message;

import hu.dmc.shared.model.generated.test.test.message.generated.GeneratedMessageManagerImpl;

/**
 * The default implementation for the manager of every {@link
 * hu.dmc.shared.model.generated.test.test.message.Message} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public final class MessageManagerImpl extends GeneratedMessageManagerImpl implements MessageManager {
    
    
}