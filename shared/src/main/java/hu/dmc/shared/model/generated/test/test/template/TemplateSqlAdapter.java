package hu.dmc.shared.model.generated.test.test.template;

import hu.dmc.shared.model.generated.test.test.template.generated.GeneratedTemplateSqlAdapter;

/**
 * The SqlAdapter for every {@link
 * hu.dmc.shared.model.generated.test.test.template.Template} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public class TemplateSqlAdapter extends GeneratedTemplateSqlAdapter {
    
    
}