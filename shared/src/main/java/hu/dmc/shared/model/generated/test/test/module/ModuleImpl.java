package hu.dmc.shared.model.generated.test.test.module;

import hu.dmc.shared.model.generated.test.test.module.generated.GeneratedModuleImpl;

/**
 * The default implementation of the {@link
 * hu.dmc.shared.model.generated.test.test.module.Module}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public final class ModuleImpl extends GeneratedModuleImpl implements Module {
    
    
}