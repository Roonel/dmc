package hu.dmc.shared.model.generated.test.test.message;

import hu.dmc.shared.model.generated.test.test.message.generated.GeneratedMessage;

/**
 * The main interface for entities of the {@code message}-table in the database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public interface Message extends GeneratedMessage {
    
    
}