package hu.dmc.shared.model.generated.test.test.template;

import hu.dmc.shared.model.generated.test.test.template.generated.GeneratedTemplate;

/**
 * The main interface for entities of the {@code template}-table in the
 * database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public interface Template extends GeneratedTemplate {
    
    
}