package hu.dmc.shared.model.generated;

import hu.dmc.shared.model.generated.generated.GeneratedSharedApplicationImpl;

/**
 * The default {@link com.speedment.runtime.core.Speedment} implementation class
 * for the {@link com.speedment.runtime.config.Project} named shared.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public final class SharedApplicationImpl extends GeneratedSharedApplicationImpl implements SharedApplication {
    
    
}