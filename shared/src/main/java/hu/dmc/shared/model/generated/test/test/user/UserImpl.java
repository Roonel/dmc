package hu.dmc.shared.model.generated.test.test.user;

import hu.dmc.shared.model.generated.test.test.user.generated.GeneratedUserImpl;

/**
 * The default implementation of the {@link
 * hu.dmc.shared.model.generated.test.test.user.User}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public final class UserImpl extends GeneratedUserImpl implements User {
    
    
}