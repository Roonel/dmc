package hu.dmc.shared.model.generated.test.test.saved_template;

import hu.dmc.shared.model.generated.test.test.saved_template.generated.GeneratedSavedTemplateManager;

/**
 * The main interface for the manager of every {@link
 * hu.dmc.shared.model.generated.test.test.saved_template.SavedTemplate} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public interface SavedTemplateManager extends GeneratedSavedTemplateManager {
    
    
}