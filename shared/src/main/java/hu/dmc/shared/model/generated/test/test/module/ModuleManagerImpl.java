package hu.dmc.shared.model.generated.test.test.module;

import hu.dmc.shared.model.generated.test.test.module.generated.GeneratedModuleManagerImpl;

/**
 * The default implementation for the manager of every {@link
 * hu.dmc.shared.model.generated.test.test.module.Module} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public final class ModuleManagerImpl extends GeneratedModuleManagerImpl implements ModuleManager {
    
    
}