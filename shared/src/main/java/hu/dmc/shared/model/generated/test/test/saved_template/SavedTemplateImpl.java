package hu.dmc.shared.model.generated.test.test.saved_template;

import hu.dmc.shared.model.generated.test.test.saved_template.generated.GeneratedSavedTemplateImpl;

/**
 * The default implementation of the {@link
 * hu.dmc.shared.model.generated.test.test.saved_template.SavedTemplate}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public final class SavedTemplateImpl extends GeneratedSavedTemplateImpl implements SavedTemplate {
    
    
}