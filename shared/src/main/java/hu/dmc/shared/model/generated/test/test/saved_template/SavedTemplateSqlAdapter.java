package hu.dmc.shared.model.generated.test.test.saved_template;

import hu.dmc.shared.model.generated.test.test.saved_template.generated.GeneratedSavedTemplateSqlAdapter;

/**
 * The SqlAdapter for every {@link
 * hu.dmc.shared.model.generated.test.test.saved_template.SavedTemplate} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public class SavedTemplateSqlAdapter extends GeneratedSavedTemplateSqlAdapter {
    
    
}