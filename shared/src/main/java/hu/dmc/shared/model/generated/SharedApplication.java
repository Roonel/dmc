package hu.dmc.shared.model.generated;

import hu.dmc.shared.model.generated.generated.GeneratedSharedApplication;

/**
 * An {@link com.speedment.runtime.core.ApplicationBuilder} interface for the
 * {@link com.speedment.runtime.config.Project} named shared.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public interface SharedApplication extends GeneratedSharedApplication {
    
    
}