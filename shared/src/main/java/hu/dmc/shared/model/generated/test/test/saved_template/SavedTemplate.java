package hu.dmc.shared.model.generated.test.test.saved_template;

import hu.dmc.shared.model.generated.test.test.saved_template.generated.GeneratedSavedTemplate;

/**
 * The main interface for entities of the {@code saved_template}-table in the
 * database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public interface SavedTemplate extends GeneratedSavedTemplate {
    
    
}