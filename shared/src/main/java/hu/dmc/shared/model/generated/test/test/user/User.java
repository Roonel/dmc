package hu.dmc.shared.model.generated.test.test.user;

import hu.dmc.shared.model.generated.test.test.user.generated.GeneratedUser;

/**
 * The main interface for entities of the {@code user}-table in the database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public interface User extends GeneratedUser {
    
    
}