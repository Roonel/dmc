package hu.dmc.shared.model.generated.test.test.module;

import hu.dmc.shared.model.generated.test.test.module.generated.GeneratedModule;

/**
 * The main interface for entities of the {@code module}-table in the database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public interface Module extends GeneratedModule {
    
    
}