package hu.dmc.shared.model.generated.test.test.user;

import hu.dmc.shared.model.generated.test.test.user.generated.GeneratedUserManager;

/**
 * The main interface for the manager of every {@link
 * hu.dmc.shared.model.generated.test.test.user.User} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public interface UserManager extends GeneratedUserManager {
    
    
}