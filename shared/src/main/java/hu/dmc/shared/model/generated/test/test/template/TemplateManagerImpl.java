package hu.dmc.shared.model.generated.test.test.template;

import hu.dmc.shared.model.generated.test.test.template.generated.GeneratedTemplateManagerImpl;

/**
 * The default implementation for the manager of every {@link
 * hu.dmc.shared.model.generated.test.test.template.Template} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author hu.dmc
 */
public final class TemplateManagerImpl extends GeneratedTemplateManagerImpl implements TemplateManager {
    
    
}